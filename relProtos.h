/****************************************************************
 *
 * $Source$
 *
 * Copyright (c) 2013
 * Bruker BioSpin MRI GmbH
 * D-76275 Ettlingen, Germany
 *
 * All Rights Reserved
 *
 * $Id$
 *
 ****************************************************************/

#ifndef METHRELS_H
#define METHRELS_H

#ifndef CPROTO
#include "relProtos_p.h"
#endif

#endif

/****************************************************************/
/*      E N D   O F   F I L E                                   */
/****************************************************************/

