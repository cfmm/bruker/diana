# -------------------------------------------------------------
# /pvCurrent/pv70/install/prog/parx/src/dianaFLASH/Makefile
# 
# Copyright (c) 2001-2011
# Bruker BioSpin MRI GmbH
# D-76275 Ettlingen, Germany
#
# All Rights Reserved
#
# To copy the directory and compile the copy it is only
# necessary to change SRCDIR.
# -------------------------------------------------------------

# -------------------------------------------------------------
#  ParaVision Linux templates
# -------------------------------------------------------------

# -------------------------------------------------------------
#  ParaVision templates
# -------------------------------------------------------------

XWINSHLIBDIR = $(PROGDIR)/shlib
XWINSHLIBDIROPT = -L$(XWINSHLIBDIR)

THIRDPARTY_SHLIBDIR64 = $(PROGDIR)/shlib64

THIRDPARTY_SHLIBDIR = $(PROGDIR)/pvshlib
THIRDPARTY_SHLIBDIROPT = -L$(THIRDPARTY_SHLIBDIR)

XWINSHARELIBEXT = $(SHARELIBEXT)

PVSHLIBDIR = $(PROGDIR)/pvshlib

MAKEDEPEND	= makedepend

RMD		= rm -rf

SHARELIBBASEOPTS = -shared

PV_EXTRA_DEFINES =

PV_DEFINES = -D_REENTRANT -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE

PV_ARCH_FLAG = -m32

PV_COMPILE_FLAGS = $(PV_ARCH_FLAG) -fPIC -rdynamic

LOCALBINDIR = $(LOCALROOT)/bin

# -------------------------------------------------------------
# Defaults for LAPACK / CLAPACK
# -------------------------------------------------------------

LAPACK_LIBRARIES = $(PVSHLIBDIR)/liblapack$(SHARELIBEXT) $(PVSHLIBDIR)/libblas$(SHARELIBEXT) $(SIMPLE_FORTRAN_LIBRARIES)

TOPSPIN_IMPORTLIBS = $(XWINSHLIBDIR)/editpar$(XWINSHARELIBEXT) $(XWINSHLIBDIR)/libacq$(XWINSHARELIBEXT)  $(XWINSHLIBDIR)/libPath$(XWINSHARELIBEXT) $(XWINSHLIBDIR)/libShapeIO$(XWINSHARELIBEXT) $(XWINSHLIBDIR)/libap$(XWINSHARELIBEXT) $(XWINSHLIBDIR)/libcb$(XWINSHARELIBEXT) $(XWINSHLIBDIR)/uni$(XWINSHARELIBEXT) $(XWINSHLIBDIR)/util$(XWINSHARELIBEXT)

TOPSPIN_PROGLIBS =

# -------------------------------------------------------------
# Define TCL definitions
# -------------------------------------------------------------

# -------------------------------------------------------------
# $Source: /bscl/CvsTree/bscl/gen/config/linux_intel.cf,v $
#
# Copyright (c) 2000
# BRUKER BIOSPIN GMBH
# D-76287 Rheinstetten, Germany
#
# All Rights Reserved
#
# $Id: linux_intel.cf,v 1.79.4.4 2012/03/12 06:06:03 pvadm Exp $
# -------------------------------------------------------------

# -------------------------------------------------------------
# $Source: /bscl/CvsTree/bscl/gen/config/gcc.def,v $
#
# Copyright (c) 2000
# BRUKER BIOSPIN GMBH
# D-76287 Rheinstetten, Germany
#
# All Rights Reserved
#
# $Id: gcc.def,v 1.105.4.3 2012/07/16 05:08:02 pvadm Exp $
# -------------------------------------------------------------

# -------------------------------------------------------------
#
# $Source: /bscl/CvsTree/bscl/gen/config/CPlusPlus.tmpl,v $
#
# Copyright (c) 1997
# BRUKER BIOSPIN GMBH
# D-76287 Rheinstetten, Germany
#
# All Rights Reserved
#
# $Id: CPlusPlus.tmpl,v 1.19 2005/09/15 08:59:28 wem Exp $
#
# Defaults for various generic parameters for C++ programs.
# You may overwrite this defaults in either dir.def, site.def
# or linux_intel.cf
# -------------------------------------------------------------

CPLUSPLUS		= $(EXECDISKUNIT)/gnu/bin/c++ -B$(EXECDISKUNIT)/gnu/lib/gcc/

STD_PLUSPLUS_LIBRARIES	= -ldl -lrt -lpthread $(EXTRA_STDLIBS)
ALL_PLUSPLUS_LIBRARIES	= $(LIBRARIES) $(EXTRA_LIBRARIES) $(STD_PLUSPLUS_LIBRARIES)

STD_PLUSPLUS_LIBPATH 	=
ALL_PLUSPLUS_LIBPATH	= $(LIBPATH) $(STD_PLUSPLUS_LIBPATH)

STD_PLUSPLUS_INCPATH 	= $(STD_INCPATH)
ALL_PLUSPLUS_INCPATH	= $(INCPATH) $(EXTRA_INCPATH) $(STD_PLUSPLUS_INCPATH)

STATICSTDCPLUSPLUS	= -static-libgcc -static-libstdc++

SHAREDSTDCPLUSPLUS	= libgcc_s.so.1 libstdc++.so.6

STD_PLUSPLUS_DEFINES 	= $(PV_EXTRA_DEFINES) $(PV_DEFINES) $(SHARED_DEFINES)
ALL_PLUSPLUS_DEFINES	= $(OS_DEFINES) $(DEFINES) $(EXTRA_DEFINES) $(DLL_DEFINES) $(STD_PLUSPLUS_DEFINES)

CORBA_SOURCE_DEFINES	=

CPLUSPLUSDEBUGFLAGS	= -O3 -DNDEBUG $(LINKTIME_OPTIMIZE)

CPLUSPLUSOPTIONS	= $(PV_COMPILE_FLAGS) -Wall -Wextra -Wpointer-arith -Wsign-compare -Wwrite-strings -Wmissing-declarations -Woverloaded-virtual -Wundef -Wdisabled-optimization -Wunused -Wlogical-op -Wuninitialized -Winit-self -std=gnu++98 -Wcast-qual -Wcast-align -Wno-variadic-macros -fimplicit-templates -fnon-call-exceptions -fno-common $(COMPILE_OPTIONS)
CPLUSPLUSLDOPTIONS	= $(PV_COMPILE_FLAGS) -fno-common -fexceptions $(LINKTIME_OPTIMIZE) $(LINK_OPTIONS) $(CPLUSPLUSDEBUGFLAGS)

CPLUSPLUSFLAGS		= $(CPLUSPLUSDEBUGFLAGS) $(CPLUSPLUSOPTIONS) $(ALL_PLUSPLUS_INCPATH) $(ALL_PLUSPLUS_DEFINES)

CPLUSPLUSLDFLAGS	= $(CPLUSPLUSLDOPTIONS) $(ALL_PLUSPLUS_LIBPATH)

PPSUFFIX		= .cc

.SUFFIXES:            # Delete the default suffixes

.PHONY : all clean cproto depend dll docu dvd emptyrule idl install release test

IMAKEFILE_DIR = $(SRCDIR)

.PHONY: Makefile64
Makefile64::

all::

define MKDIR_FCN

ifeq ($$(filter $(1),$$(PV_CREATE_DIRS)),)
PV_CREATE_DIRS += $(1)

$$(strip $(1)):
	$$(MKDIR) $(1)

endif
endef

# -------------------------------------------------------------
# C++ rules
# -------------------------------------------------------------

# -------------------------------------------------------------
# platform-specific configuration parameters - edit
# linux_intel.cf to change
# -------------------------------------------------------------

# -------------------------------------------------------------
# $Source: /bscl/CvsTree/bscl/gen/config/linux_intel.cf,v $
#
# Copyright (c) 2000
# BRUKER BIOSPIN GMBH
# D-76287 Rheinstetten, Germany
#
# All Rights Reserved
#
# $Id: linux_intel.cf,v 1.79.4.4 2012/03/12 06:06:03 pvadm Exp $
# -------------------------------------------------------------

# -------------------------------------------------------------
# $Source: /bscl/CvsTree/bscl/gen/config/gcc.def,v $
#
# Copyright (c) 2000
# BRUKER BIOSPIN GMBH
# D-76287 Rheinstetten, Germany
#
# All Rights Reserved
#
# $Id: gcc.def,v 1.105.4.3 2012/07/16 05:08:02 pvadm Exp $
# -------------------------------------------------------------

# -------------------------------------------------------------
# Defaults for various generic parameters defined in
# "/pvCurrent/pv70/gen/config/ParxExport.tmpl". You may overwrite this defaults in either
# dir.def, site.def or linux_intel.cf
# -------------------------------------------------------------

AWK		= awk

BASENAME	= basename

BASH		= bash

CAT		= cat

CC		= $(EXECDISKUNIT)/gnu/bin/gcc -B$(EXECDISKUNIT)/gnu/lib/gcc/

CHMOD		= chmod

CKSUM		= cksum

CMP		= cmp

CP		= cp

CP_R		= cp -r

CPASC		= cp

CUT		= cut

DATE		= date

DIFF		= diff

ECHO		= /bin/echo -e

EXECSTACK	= execstack

FIND		= find

GREP		= grep

GTAR		= $(ARCH_LOCAL)/tar-1.26/bin/tar

HEAD		= head

LN		= ln

LS		= ls

M4		= m4

MAKE		= /usr/local/make-4.1/bin/make

MAKEDEPEND	= makedepend

MKDIR		= mkdir -p

MV		= mv

PERL		= perl

RCP		= rcp

SCP		= scp

RM		= rm -f

RMDIR		= rm -rf

SED		= sed

SHARELIBCMD	=

SHELL		= sh

TAR		= $(ARCH_LOCAL)/tar-1.26/bin/tar

TOUCH		= touch

UNAME		= uname

WHOAMI		= whoami

NOOP		= true

STRIP		= $(CC_ROOT)/bin/strip

SORT		= sort -u

XXD		= xxd

DOS2UNIX	= dos2unix

SSH		= ssh

COMM		= comm

ZIPCMD		= gzip -f

UNZIPCMD	= gunzip -f

TOP		= .
CURRENT_DIR	= .

RM_CMD		= $(RM) *.CKP *.ln *.BAK *.bak *.o core errs ,* *~ *.a *.lib .emacs_* tags TAGS make.log MakeOut

APP_SUBSYSTEM	=

STD_LIBRARIES	= -ldl -lrt -lpthread $(EXTRA_STDLIBS)
ALL_LIBRARIES	= $(LIBRARIES) $(EXTRA_LIBRARIES) $(STD_LIBRARIES)

STD_LIBPATH	=
ALL_LIBPATH	= $(LIBPATH) $(STD_LIBPATH)

STD_INCPATH	= -I $(EXECDISKUNIT)/prog/include
ALL_INCPATH	= $(INCPATH) $(EXTRA_INCPATH) $(STD_INCPATH)

STD_DEFINES	= $(PV_EXTRA_DEFINES) $(PV_DEFINES) $(SHARED_DEFINES)
OS_DEFINES	= -DLINUX_INTEL
DLL_DEFINES	=
ALL_DEFINES	= $(OS_DEFINES) $(DEFINES) $(EXTRA_DEFINES) $(DLL_DEFINES) $(STD_DEFINES)

CDEBUGFLAGS	= -O3 -DNDEBUG $(LINKTIME_OPTIMIZE)

CCOPTIONS	= $(PV_COMPILE_FLAGS) -Wall -Wextra  -Wpointer-arith -Wsign-compare -Wwrite-strings -Wstrict-prototypes -Wmissing-prototypes -Wmissing-declarations -Wnested-externs -Winline -Wundef -Wunused -Wdisabled-optimization -Wlogical-op -pedantic -Wuninitialized -Winit-self -std=gnu99 -Wconversion -Wcast-qual -Wcast-align -fexceptions -fno-common $(COMPILE_OPTIONS)

CCLDOPTIONS	= $(PV_COMPILE_FLAGS) -fno-common -fexceptions $(LINKTIME_OPTIMIZE) $(LINK_OPTIONS)

LDDEBUGFLAGS	=

LDOPTIONS	=

DLLLDOPTIONS	=

CFLAGS		= $(CDEBUGFLAGS) $(CCOPTIONS) $(ALL_INCPATH) $(ALL_DEFINES)
CCLDFLAGS	= $(CDEBUGFLAGS) $(CCOPTIONS) $(CCLDOPTIONS) $(ALL_LIBPATH)

EXECEXT		=

OBJEXT		= .o

STATICLIBEXT	= .a

STATICCRTLIBEXT	= .a

DYNAMICLIBEXT	= .so

ASMEXT		= .asm

ROMEXT		= .rom

ZIPEXT		= .gz

SHARELIBEXT	= .so

CPROTO		= $(EXECDISKUNIT)/prog/bin/cproto
CPROTOOPTIONS	= -E"$(CC) -E" -f2
CPROTO_DEFINES	= -DCPROTO
CPROTOFLAGS	= $(CPROTOOPTIONS) $(CPROTO_DEFINES) $(ALL_DEFINES) $(subst -isystem, -I, $(ALL_INCPATH))

MATH_LIBRARIES	= -lm

PTHREAD_LIBRARIES= -lpthread

# -------------------------------------------------------------
#
# $Source: /bscl/CvsTree/bscl/gen/config/sca.rules,v $
#
# Copyright (c) 2007
# BRUKER BIOSPIN GMBH
# D-76287 Rheinstetten, Germany
#
# All Rights Reserved
#
# $Id: sca.rules,v 1.15 2010/05/21 13:15:18 jgo Exp $
# -------------------------------------------------------------

# -------------------------------------------------------------
# rules for static code analysis:
# -------------------------------------------------------------

# -------------------------------------------------------------
# $Source: /bscl/CvsTree/bscl/gen/config/Standard.tmpl,v $
#
# Copyright (c) 1995
# BRUKER BIOSPIN GMBH
# D-76287 Rheinstetten, Germany
#
# All Rights Reserved
#
# $Id: Standard.tmpl,v 1.27 2009/12/07 07:16:54 wem Exp $
# -------------------------------------------------------------

# -------------------------------------------------------------
# Standard build parameters
# -------------------------------------------------------------

EXECDISKUNIT	= /opt/PV-7.0.0

PROGDIR		= $(EXECDISKUNIT)/prog

STANDALONEDIR	= $(EXECDISKUNIT)/prog/bin

SHLIBDIR	= $(PROGDIR)/pvshlib

DLLDIR		= $(PROGDIR)/pvshlib

# For compatibilty to old Imakefiles - do not use !

PROTOHEADERDIR	= $(EXECDISKUNIT)/prog/include/proto

TMPDIR		= /tmp

DEPENDFLAGS	= -DMAKEDEPEND

# -------------------------------------------------------------
# Standard rule set
# -------------------------------------------------------------

DEPEND_START = \# DO NOT DELETE THIS LINE -- make depend depends on it

# -------------------------------------------------------------
# Start of Imakefile
# -------------------------------------------------------------

PARCOMP_OPTS     =
PARCOMP_DEFINES  = -DInParxOvl
PARCOMP_FLAGS	 = $(PARX_EXTRA_FLAGS) $(ALL_INCPATH) $(DEFINES) $(PARCOMP_DEFINES) $(PARCOMP_OPTS) $(PARCOMP_DEBUG)
PARCOMP_DEBUG    =
PARCOMP	         = $(EXECDISKUNIT)/prog/bin/scripts/methcomp

PARX_OVT_FLAGS	 =
PARX_OVE_FLAGS	 = -P $(PULPROG) $(PULPROG_INC)

PARX_OVL_LIBPATH = -L$(SHLIBDIR) -L$(XWINSHLIBDIR)
PARX_OVL_LIBS	 = $(LIBS)
PARX_OVL_LIBRARIES = $(LIBRARIES) $(STD_LIBRARIES)
PARX_OVL_CPP     =
PARX_OVL_LD      = $(EXECDISKUNIT)/gnu/bin/c++ -B$(EXECDISKUNIT)/gnu/lib/gcc/
PARX_OVL_EXT     = .so
PARX_OVL_AR      = $(AR)
PARX_OVL_ARFLAGS =
PARX_TK_EXT      = $(SHARELIBEXT)

PARX_PP_STD_INCL = -p $(PPG_DIR) -p $(EXP_LISTS_DIR)/pp -p $(EXP_LISTS_DIR)/pp.dtomo -p $(EXP_LISTS_DIR)/pp.dexam

METHODS_DIR	 = $(EXECDISKUNIT)/prog/curdir/$(USER)/ParaVision/methods

EXP_LISTS_DIR    = $(EXECDISKUNIT)/exp/stan/nmr/lists
PPG_DIR	 = $(EXECDISKUNIT)/prog/curdir/$(USER)/ParaVision/exp/lists/pp
EXTRA_DEFINES    = $(PARCOMP_DEFINES)

.PHONY: instclean instlist

installppg:

.SILENT:

forceppg:

.PHONY: forceppg

all::
showppg ::
	@if [ "$(PULPROG)" != "none" ]; then echo $(notdir $PULPROG); fi

OVERLAY		= dianaFLASH
PULPROG		= none
PULPROG_INC	=

SRCDIR		= $(EXECDISKUNIT)/prog/curdir/$(USER)/ParaVision/methods/src/dianaFLASH

INCPATH		= -I.

RELOBJS		=	\
			initMeth$(OBJEXT) \
			loadMeth$(OBJEXT) \
			parsRelations$(OBJEXT) \
			BaseLevelRelations$(OBJEXT) \
			RecoRelations$(OBJEXT) \
			backbone$(OBJEXT) \
			deriveVisu$(OBJEXT) 

OBJS		= 		  $(OVERLAY)$(OBJEXT) 		  $(RELOBJS)

SRCS		= $(addprefix $(SRCDIR)/, $(addsuffix  .c,  $(basename $(notdir $(OBJS)))))

RELSRCS		= $(addprefix $(SRCDIR)/, $(addsuffix  .c,  $(basename $(notdir $(RELOBJS)))))

LIBS	        = 		  $(SHLIBDIR)/libPvUtil$(SHARELIBEXT) 		  $(SHLIBDIR)/libPvUtilTools$(SHARELIBEXT)                   $(SHLIBDIR)/libPvCfgTools$(SHARELIBEXT) 		  $(SHLIBDIR)/libPvAcqTools$(SHARELIBEXT) 		  $(SHLIBDIR)/libPvPvmTools$(SHARELIBEXT) 		  $(SHLIBDIR)/libPvMrTools$(SHARELIBEXT) 		  $(SHLIBDIR)/libPvGeoTools$(SHARELIBEXT) 		  $(SHLIBDIR)/libPvSeqTools$(SHARELIBEXT) 		  $(SHLIBDIR)/libPvPvmTools$(SHARELIBEXT)                   $(SHLIBDIR)/libPvOvlTools$(SHARELIBEXT) 		  $(SHLIBDIR)/libParxRels$(SHARELIBEXT) 		  $(SHLIBDIR)/libParxBase$(SHARELIBEXT)

LIBRARIES	= 		  -lm

all::	$(OVERLAY)$(PARX_OVL_EXT)

$(eval $(call MKDIR_FCN,$(abspath $(dir $(METHODS_DIR)/$(notdir $(OVERLAY)$(PARX_OVL_EXT))))))
$(METHODS_DIR)/$(notdir $(OVERLAY)$(PARX_OVL_EXT)): $(OVERLAY)$(PARX_OVL_EXT) | $(abspath $(dir $(METHODS_DIR)/$(notdir $(OVERLAY)$(PARX_OVL_EXT))))
	$(info Copy $(OVERLAY)$(PARX_OVL_EXT) to $(METHODS_DIR)/$(notdir $(OVERLAY)$(PARX_OVL_EXT)))
	$(RM) $(METHODS_DIR)/$(notdir $(OVERLAY)$(PARX_OVL_EXT))
	$(CP) $(OVERLAY)$(PARX_OVL_EXT) $(METHODS_DIR)/$(notdir $(OVERLAY)$(PARX_OVL_EXT))
clean::
	$(RM) $(METHODS_DIR)/$(notdir $(OVERLAY)$(PARX_OVL_EXT))
install:: $(METHODS_DIR)/$(notdir $(OVERLAY)$(PARX_OVL_EXT))

instclean: instclean$(PARX_OVL_EXT)

.PHONY : instclean$(PARX_OVL_EXT)
instclean$(PARX_OVL_EXT):
	$(RM) $(METHODS_DIR)/$(OVERLAY)$(PARX_OVL_EXT)

instlist: instlist$(PARX_OVL_EXT)

.PHONY : instlist$(PARX_OVL_EXT)
instlist$(PARX_OVL_EXT):
	@echo $(METHODS_DIR)/$(OVERLAY)$(PARX_OVL_EXT)
clean:: instclean

$(OVERLAY)$(PARX_OVL_EXT): $(OBJS) $(PARX_OVL_LIBS)
	@$(ECHO) Link $(OVERLAY)$(PARX_OVL_EXT)
	$(RM) $(OVERLAY)$(PARX_OVL_EXT)
	$(PARX_OVL_LD) -shared -Wl,-no-undefined,-soname,$(OVERLAY)$(PARX_OVL_EXT) \
	   $(CPLUSPLUSLDOPTIONS) $(OBJS) \
	   $(PARX_OVL_LIBPATH)  $(PARX_OVL_LIBS) \
	   $(PARX_OVL_LIBRARIES) -o $(OVERLAY)$(PARX_OVL_EXT)

DEP_SRCS += $(SRCS)
DEP_LINES += " "
depend::
	$(RM) Makefile.bak
	$(MV) Makefile Makefile.bak
	@$(SED) '/^$(DEPEND_START)/,$$d' < Makefile.bak > Makefile
	@$(ECHO) "$(DEPEND_START)" >> Makefile
	$(CPLUSPLUS) -MM $(CPLUSPLUSFLAGS) $(DEPENDFLAGS) $(DEP_SRCS) >> Makefile
	for line in $(DEP_LINES); do echo $$line >> Makefile; done

$(RELOBJS): $(OVERLAY)$(OBJEXT)
	@$(ECHO) Compile $(SRCDIR)/$(@:$(OBJEXT)=.c)
	$(PARCOMP) $(SRCDIR)/$(OVERLAY).c $(PARCOMP_FLAGS) -r $(SRCDIR)/$(@:$(OBJEXT)=.c)

$(OVERLAY)$(OBJEXT):
	@$(ECHO) Compile $(SRCDIR)/$(OVERLAY).c
	$(PARCOMP) $(SRCDIR)/$(OVERLAY).c   $(PARX_OVE_FLAGS) $(PARCOMP_FLAGS)

ifeq ($(SRCDIR)/$(OVERLAY).xml,$(wildcard $(SRCDIR)/$(OVERLAY).xml))
install:: $(METHODS_DIR)/$(OVERLAY).xml

$(eval $(call MKDIR_FCN,$(METHODS_DIR)))
$(METHODS_DIR)/$(OVERLAY).xml : $(SRCDIR)/$(OVERLAY).xml | $(METHODS_DIR)
	$(info Install $(METHODS_DIR)/$(OVERLAY).xml)
	$(RM) $(METHODS_DIR)/$(OVERLAY).xml
	$(CPASC) $(SRCDIR)/$(OVERLAY).xml $(METHODS_DIR)/$(OVERLAY).xml

instclean: instclean.xml

.PHONY : instclean.xml
instclean.xml:
	$(RM) $(METHODS_DIR)/$(OVERLAY).xml

instlist: instlist.xml

.PHONY : instlist.xml
instlist.xml:
	@echo $(METHODS_DIR)/$(OVERLAY).xml
clean:: instclean
endif

ifeq ($(SRCDIR)/$(OVERLAY).html,$(wildcard $(SRCDIR)/$(OVERLAY).html))
install:: $(METHODS_DIR)/$(OVERLAY).html

$(eval $(call MKDIR_FCN,$(METHODS_DIR)))
$(METHODS_DIR)/$(OVERLAY).html : $(SRCDIR)/$(OVERLAY).html | $(METHODS_DIR)
	$(info Install $(METHODS_DIR)/$(OVERLAY).html)
	$(RM) $(METHODS_DIR)/$(OVERLAY).html
	$(CPASC) $(SRCDIR)/$(OVERLAY).html $(METHODS_DIR)/$(OVERLAY).html

instclean: instclean.html

.PHONY : instclean.html
instclean.html:
	$(RM) $(METHODS_DIR)/$(OVERLAY).html

instlist: instlist.html

.PHONY : instlist.html
instlist.html:
	@echo $(METHODS_DIR)/$(OVERLAY).html
clean:: instclean
endif

installppg: $(PPG_DIR)/$(notdir $(SRCDIR)/$(OVERLAY).ppg)

$(eval $(call MKDIR_FCN,$(PPG_DIR)))
$(PPG_DIR)/$(notdir $(SRCDIR)/$(OVERLAY).ppg) : forceppg | $(PPG_DIR)
	$(RM) $(PPG_DIR)/$(notdir $(SRCDIR)/$(OVERLAY).ppg)
	$(info Install $(PPG_DIR)/$(notdir $(SRCDIR)/$(OVERLAY).ppg))
	$(CP) $(SRCDIR)/$(OVERLAY).ppg $(PPG_DIR)/$(notdir $(SRCDIR)/$(OVERLAY).ppg)

instclean: instclean$(notdir $(SRCDIR)/$(OVERLAY).ppg)

.PHONY : instclean$(notdir $(SRCDIR)/$(OVERLAY).ppg)
instclean$(notdir $(SRCDIR)/$(OVERLAY).ppg):
	$(RM) $(PPG_DIR)/$(notdir $(SRCDIR)/$(OVERLAY).ppg)

instlist: instlist$(notdir $(SRCDIR)/$(OVERLAY).ppg)

.PHONY : instlist$(notdir $(SRCDIR)/$(OVERLAY).ppg)
instlist$(notdir $(SRCDIR)/$(OVERLAY).ppg):
	@echo $(PPG_DIR)/$(notdir $(SRCDIR)/$(OVERLAY).ppg)
clean:: instclean

showppg ::
	@echo $(notdir $(SRCDIR)/$(OVERLAY).ppg)

installppg: $(PPG_DIR)/$(notdir $(SRCDIR)/$(OVERLAY)Angio.ppg)

$(eval $(call MKDIR_FCN,$(PPG_DIR)))
$(PPG_DIR)/$(notdir $(SRCDIR)/$(OVERLAY)Angio.ppg) : forceppg | $(PPG_DIR)
	$(RM) $(PPG_DIR)/$(notdir $(SRCDIR)/$(OVERLAY)Angio.ppg)
	$(info Install $(PPG_DIR)/$(notdir $(SRCDIR)/$(OVERLAY)Angio.ppg))
	$(CP) $(SRCDIR)/$(OVERLAY)Angio.ppg $(PPG_DIR)/$(notdir $(SRCDIR)/$(OVERLAY)Angio.ppg)

instclean: instclean$(notdir $(SRCDIR)/$(OVERLAY)Angio.ppg)

.PHONY : instclean$(notdir $(SRCDIR)/$(OVERLAY)Angio.ppg)
instclean$(notdir $(SRCDIR)/$(OVERLAY)Angio.ppg):
	$(RM) $(PPG_DIR)/$(notdir $(SRCDIR)/$(OVERLAY)Angio.ppg)

instlist: instlist$(notdir $(SRCDIR)/$(OVERLAY)Angio.ppg)

.PHONY : instlist$(notdir $(SRCDIR)/$(OVERLAY)Angio.ppg)
instlist$(notdir $(SRCDIR)/$(OVERLAY)Angio.ppg):
	@echo $(PPG_DIR)/$(notdir $(SRCDIR)/$(OVERLAY)Angio.ppg)
clean:: instclean

showppg ::
	@echo $(notdir $(SRCDIR)/$(OVERLAY)Angio.ppg)

cproto::
	-if [ ! -r relProtos_p.h ]; then			  		\
		$(TOUCH) relProtos_p.h;				\
	fi
	$(CPROTO) $(CPROTOFLAGS) -o relProtos.new -O relProtos.err $(RELSRCS)
	cat relProtos.err
	file=relProtos.err; test -z "`cat $$file`"
	$(CMP) -s relProtos relProtos.new || \
	{ \
		$(ECHO) "Install new prototype file"; \
		$(RM) relProtos_p.h; \
		$(MV) relProtos.new relProtos_p.h; \
	}
	$(RM) relProtos.new
	$(RM) relProtos.err

# -------------------------------------------------------------
# Common rules for all Makefiles - do not edit
# -------------------------------------------------------------

emptyrule::

dvd::

clean::
	$(RM_CMD)

# -------------------------------------------------------------
# Empty rules for directories that do not have SUBDIRS
# -------------------------------------------------------------

depend::

Makefiles::

all::

test::

sca::

install::

cproto::

dll::

idl::

docu::

# -------------------------------------------------------------
# Dependencies generated by makedepend
# -------------------------------------------------------------

# DO NOT DELETE THIS LINE -- make depend depends on it
dianaFLASH.o: \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/dianaFLASH.c \
 /opt/PV-7.0.0/prog/include/bruktyp.h \
 /opt/PV-7.0.0/prog/include/acqutyp.h \
 /opt/PV-7.0.0/prog/include/gradient_ramp_typ.h \
 /opt/PV-7.0.0/prog/include/acqumtyp.h \
 /opt/PV-7.0.0/prog/include/acqutyp.h \
 /opt/PV-7.0.0/prog/include/Parx/Dynenum.h \
 /opt/PV-7.0.0/prog/include/recotyp.h \
 /opt/PV-7.0.0/prog/include/Reco/RecoStageTyp.h \
 /opt/PV-7.0.0/prog/include/subjtyp.h \
 /opt/PV-7.0.0/prog/include/generated/ParxDefs.h \
 /opt/PV-7.0.0/prog/include/ta_config.h \
 /opt/PV-7.0.0/prog/include/methodTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/publicPvmTypes.h \
 /opt/PV-7.0.0/prog/include/Parx/Dynenum.h \
 /opt/PV-7.0.0/prog/include/lib/PvSysinfoClient_state.h \
 /opt/PV-7.0.0/prog/include/adjManagerTypes.h \
 /opt/PV-7.0.0/prog/include/bruktyp.h \
 /opt/PV-7.0.0/prog/include/PvUtil/TimeDefs.h \
 /opt/PV-7.0.0/prog/include/adjManagerDefs.h \
 /opt/PV-7.0.0/prog/include/generated/DataPath.h \
 /opt/PV-7.0.0/prog/include/generated/KeyUids.h \
 /opt/PV-7.0.0/prog/include/PvCfg/ResultParsType.h \
 /opt/PV-7.0.0/prog/include/PvUtil/TimeDefs.h \
 /opt/PV-7.0.0/prog/include/generated/DataPath.h \
 /opt/PV-7.0.0/prog/include/generated/KeyUids.h \
 /opt/PV-7.0.0/prog/include/adjManagerDefs.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/geotyp.h \
 /opt/PV-7.0.0/prog/include/pvidl.h \
 /opt/PV-7.0.0/prog/include/generated/GeoObjIds.h \
 /opt/PV-7.0.0/prog/include/generated/ParxDefs.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/ReportTypes.h \
 /opt/PV-7.0.0/prog/include/generated/ParImport.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/wobbleTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/epiTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/SpiralTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/TrajectoryTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/internalPvmTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/SpiralTypesInternal.h \
 /opt/PV-7.0.0/prog/include/Visu/VisuTypes.h \
 /opt/PV-7.0.0/prog/include/Visu/VisuDefines.h \
 /opt/PV-7.0.0/prog/include/generated/VisuIds.h \
 /opt/PV-7.0.0/prog/include/proto/acq_extern.h \
 /opt/PV-7.0.0/prog/include/proto/subj_extern.h \
 /opt/PV-7.0.0/prog/include/proto/pvm_extern.h \
 /opt/PV-7.0.0/prog/include/proto/visu_extern.h \
 /opt/PV-7.0.0/prog/include/methodFormat.h \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/parsTypes.h \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/parsDefinition.h \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/callbackDefs.h \
 /opt/PV-7.0.0/prog/include/methodClassDefs.h \
 /opt/PV-7.0.0/prog/include/digitizerClassDefs.h \
 /opt/PV-7.0.0/prog/include/epiClassDefs.h \
 /opt/PV-7.0.0/prog/include/SpiralCls.h \
 /opt/PV-7.0.0/prog/include/TrajectoryCls.h \
 /opt/PV-7.0.0/prog/include/diffusionClassDefs.h \
 /opt/PV-7.0.0/prog/include/modulesClassDefs.h \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/parsLayout.h
initMeth.o: \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/initMeth.c \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/method.h \
 /opt/PV-7.0.0/prog/include/pvmachine.h \
 /opt/PV-7.0.0/prog/include/machine.h /opt/PV-7.0.0/prog/include/pvidl.h \
 /opt/PV-7.0.0/prog/include/generated/PvSystem.h \
 /opt/PV-7.0.0/prog/include/brukdef.h \
 /opt/PV-7.0.0/prog/include/bruktyp.h \
 /opt/PV-7.0.0/prog/include/Parx/Parx.h \
 /opt/PV-7.0.0/prog/include/Parx/publicTypes.h \
 /opt/PV-7.0.0/prog/include/PvUtil/TimeDefs.h \
 /opt/PV-7.0.0/prog/include/generated/ParxDefs.h \
 /opt/PV-7.0.0/prog/include/Parx/libParxRels.h \
 /opt/PV-7.0.0/prog/include/Parx/ovlinterface.h \
 /opt/PV-7.0.0/prog/include/Parx/pardef.h \
 /opt/PV-7.0.0/prog/include/generated/KeyUids.h \
 /opt/PV-7.0.0/prog/include/PvUtil/perfhash.h \
 /opt/PV-7.0.0/prog/include/ta_config.h \
 /opt/PV-7.0.0/prog/include/Parx/Dynenum.h \
 /opt/PV-7.0.0/prog/include/debugdef.h \
 /opt/PV-7.0.0/prog/include/lib/libPvPath.h \
 /opt/PV-7.0.0/prog/include/generated/DataPath.h \
 /opt/PV-7.0.0/prog/include/acqutyp.h \
 /opt/PV-7.0.0/prog/include/gradient_ramp_typ.h \
 /opt/PV-7.0.0/prog/include/acqumtyp.h \
 /opt/PV-7.0.0/prog/include/acqutyp.h \
 /opt/PV-7.0.0/prog/include/recotyp.h \
 /opt/PV-7.0.0/prog/include/Reco/RecoStageTyp.h \
 /opt/PV-7.0.0/prog/include/subjtyp.h \
 /opt/PV-7.0.0/prog/include/generated/ParxDefs.h \
 /opt/PV-7.0.0/prog/include/methodTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/publicPvmTypes.h \
 /opt/PV-7.0.0/prog/include/Parx/Dynenum.h \
 /opt/PV-7.0.0/prog/include/lib/PvSysinfoClient_state.h \
 /opt/PV-7.0.0/prog/include/adjManagerTypes.h \
 /opt/PV-7.0.0/prog/include/bruktyp.h \
 /opt/PV-7.0.0/prog/include/PvUtil/TimeDefs.h \
 /opt/PV-7.0.0/prog/include/adjManagerDefs.h \
 /opt/PV-7.0.0/prog/include/generated/DataPath.h \
 /opt/PV-7.0.0/prog/include/generated/KeyUids.h \
 /opt/PV-7.0.0/prog/include/PvCfg/ResultParsType.h \
 /opt/PV-7.0.0/prog/include/adjManagerDefs.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/geotyp.h \
 /opt/PV-7.0.0/prog/include/pvidl.h \
 /opt/PV-7.0.0/prog/include/generated/GeoObjIds.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/ReportTypes.h \
 /opt/PV-7.0.0/prog/include/generated/ParImport.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/wobbleTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/epiTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/SpiralTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/TrajectoryTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/internalPvmTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/SpiralTypesInternal.h \
 /opt/PV-7.0.0/prog/include/PVMTools.h \
 /opt/PV-7.0.0/prog/include/recotyp.h \
 /opt/PV-7.0.0/prog/include/PvUtilTools.h \
 /opt/PV-7.0.0/prog/include/PvUtil/PvExcept.hh \
 /opt/PV-7.0.0/prog/include/PvUtil/PvUtil.h \
 /opt/PV-7.0.0/prog/include/machine.h \
 /opt/PV-7.0.0/prog/include/Parx/RelationContext.hh \
 /opt/PV-7.0.0/prog/include/PvAcqTools.h \
 /opt/PV-7.0.0/prog/include/PvCfgTools.h \
 /opt/PV-7.0.0/prog/include/PvCfg/PvCfg.h \
 /opt/PV-7.0.0/prog/include/PvGeoTools.h \
 /opt/PV-7.0.0/prog/include/PvGeoClasses.hh \
 /opt/PV-7.0.0/prog/include/PvmTypes/geotyp.h \
 /opt/PV-7.0.0/prog/include/libCore/Math/Matrix3x3.hh \
 /opt/PV-7.0.0/prog/include/libCore/Containers/Exceptions/MathException.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/ExceptionBase.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/Core.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/internal.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/String.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/CoreString.h \
 /opt/PV-7.0.0/prog/include/libCore/Base/TimeStamped.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/TimeStamp.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/AutoPtr.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/Cleanup.hh \
 /opt/PV-7.0.0/prog/include/PvMrTools.h \
 /opt/PV-7.0.0/prog/include/PvPvmTools.h \
 /opt/PV-7.0.0/prog/include/adjManagerTypes.h \
 /opt/PV-7.0.0/prog/include/PvSeqTools.h \
 /opt/PV-7.0.0/prog/include/ovl_toolbox/RecoTools.h \
 /opt/PV-7.0.0/prog/include/ovl_toolbox/VisuTools.h \
 /opt/PV-7.0.0/prog/include/ovl_toolbox/Utils.h \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/parsTypes.h \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/relProtos.h \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/relProtos_p.h
loadMeth.o: \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/loadMeth.c \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/method.h \
 /opt/PV-7.0.0/prog/include/pvmachine.h \
 /opt/PV-7.0.0/prog/include/machine.h /opt/PV-7.0.0/prog/include/pvidl.h \
 /opt/PV-7.0.0/prog/include/generated/PvSystem.h \
 /opt/PV-7.0.0/prog/include/brukdef.h \
 /opt/PV-7.0.0/prog/include/bruktyp.h \
 /opt/PV-7.0.0/prog/include/Parx/Parx.h \
 /opt/PV-7.0.0/prog/include/Parx/publicTypes.h \
 /opt/PV-7.0.0/prog/include/PvUtil/TimeDefs.h \
 /opt/PV-7.0.0/prog/include/generated/ParxDefs.h \
 /opt/PV-7.0.0/prog/include/Parx/libParxRels.h \
 /opt/PV-7.0.0/prog/include/Parx/ovlinterface.h \
 /opt/PV-7.0.0/prog/include/Parx/pardef.h \
 /opt/PV-7.0.0/prog/include/generated/KeyUids.h \
 /opt/PV-7.0.0/prog/include/PvUtil/perfhash.h \
 /opt/PV-7.0.0/prog/include/ta_config.h \
 /opt/PV-7.0.0/prog/include/Parx/Dynenum.h \
 /opt/PV-7.0.0/prog/include/debugdef.h \
 /opt/PV-7.0.0/prog/include/lib/libPvPath.h \
 /opt/PV-7.0.0/prog/include/generated/DataPath.h \
 /opt/PV-7.0.0/prog/include/acqutyp.h \
 /opt/PV-7.0.0/prog/include/gradient_ramp_typ.h \
 /opt/PV-7.0.0/prog/include/acqumtyp.h \
 /opt/PV-7.0.0/prog/include/acqutyp.h \
 /opt/PV-7.0.0/prog/include/recotyp.h \
 /opt/PV-7.0.0/prog/include/Reco/RecoStageTyp.h \
 /opt/PV-7.0.0/prog/include/subjtyp.h \
 /opt/PV-7.0.0/prog/include/generated/ParxDefs.h \
 /opt/PV-7.0.0/prog/include/methodTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/publicPvmTypes.h \
 /opt/PV-7.0.0/prog/include/Parx/Dynenum.h \
 /opt/PV-7.0.0/prog/include/lib/PvSysinfoClient_state.h \
 /opt/PV-7.0.0/prog/include/adjManagerTypes.h \
 /opt/PV-7.0.0/prog/include/bruktyp.h \
 /opt/PV-7.0.0/prog/include/PvUtil/TimeDefs.h \
 /opt/PV-7.0.0/prog/include/adjManagerDefs.h \
 /opt/PV-7.0.0/prog/include/generated/DataPath.h \
 /opt/PV-7.0.0/prog/include/generated/KeyUids.h \
 /opt/PV-7.0.0/prog/include/PvCfg/ResultParsType.h \
 /opt/PV-7.0.0/prog/include/adjManagerDefs.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/geotyp.h \
 /opt/PV-7.0.0/prog/include/pvidl.h \
 /opt/PV-7.0.0/prog/include/generated/GeoObjIds.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/ReportTypes.h \
 /opt/PV-7.0.0/prog/include/generated/ParImport.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/wobbleTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/epiTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/SpiralTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/TrajectoryTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/internalPvmTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/SpiralTypesInternal.h \
 /opt/PV-7.0.0/prog/include/PVMTools.h \
 /opt/PV-7.0.0/prog/include/recotyp.h \
 /opt/PV-7.0.0/prog/include/PvUtilTools.h \
 /opt/PV-7.0.0/prog/include/PvUtil/PvExcept.hh \
 /opt/PV-7.0.0/prog/include/PvUtil/PvUtil.h \
 /opt/PV-7.0.0/prog/include/machine.h \
 /opt/PV-7.0.0/prog/include/Parx/RelationContext.hh \
 /opt/PV-7.0.0/prog/include/PvAcqTools.h \
 /opt/PV-7.0.0/prog/include/PvCfgTools.h \
 /opt/PV-7.0.0/prog/include/PvCfg/PvCfg.h \
 /opt/PV-7.0.0/prog/include/PvGeoTools.h \
 /opt/PV-7.0.0/prog/include/PvGeoClasses.hh \
 /opt/PV-7.0.0/prog/include/PvmTypes/geotyp.h \
 /opt/PV-7.0.0/prog/include/libCore/Math/Matrix3x3.hh \
 /opt/PV-7.0.0/prog/include/libCore/Containers/Exceptions/MathException.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/ExceptionBase.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/Core.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/internal.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/String.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/CoreString.h \
 /opt/PV-7.0.0/prog/include/libCore/Base/TimeStamped.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/TimeStamp.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/AutoPtr.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/Cleanup.hh \
 /opt/PV-7.0.0/prog/include/PvMrTools.h \
 /opt/PV-7.0.0/prog/include/PvPvmTools.h \
 /opt/PV-7.0.0/prog/include/adjManagerTypes.h \
 /opt/PV-7.0.0/prog/include/PvSeqTools.h \
 /opt/PV-7.0.0/prog/include/ovl_toolbox/RecoTools.h \
 /opt/PV-7.0.0/prog/include/ovl_toolbox/VisuTools.h \
 /opt/PV-7.0.0/prog/include/ovl_toolbox/Utils.h \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/parsTypes.h \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/relProtos.h \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/relProtos_p.h
parsRelations.o: \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/parsRelations.c \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/method.h \
 /opt/PV-7.0.0/prog/include/pvmachine.h \
 /opt/PV-7.0.0/prog/include/machine.h /opt/PV-7.0.0/prog/include/pvidl.h \
 /opt/PV-7.0.0/prog/include/generated/PvSystem.h \
 /opt/PV-7.0.0/prog/include/brukdef.h \
 /opt/PV-7.0.0/prog/include/bruktyp.h \
 /opt/PV-7.0.0/prog/include/Parx/Parx.h \
 /opt/PV-7.0.0/prog/include/Parx/publicTypes.h \
 /opt/PV-7.0.0/prog/include/PvUtil/TimeDefs.h \
 /opt/PV-7.0.0/prog/include/generated/ParxDefs.h \
 /opt/PV-7.0.0/prog/include/Parx/libParxRels.h \
 /opt/PV-7.0.0/prog/include/Parx/ovlinterface.h \
 /opt/PV-7.0.0/prog/include/Parx/pardef.h \
 /opt/PV-7.0.0/prog/include/generated/KeyUids.h \
 /opt/PV-7.0.0/prog/include/PvUtil/perfhash.h \
 /opt/PV-7.0.0/prog/include/ta_config.h \
 /opt/PV-7.0.0/prog/include/Parx/Dynenum.h \
 /opt/PV-7.0.0/prog/include/debugdef.h \
 /opt/PV-7.0.0/prog/include/lib/libPvPath.h \
 /opt/PV-7.0.0/prog/include/generated/DataPath.h \
 /opt/PV-7.0.0/prog/include/acqutyp.h \
 /opt/PV-7.0.0/prog/include/gradient_ramp_typ.h \
 /opt/PV-7.0.0/prog/include/acqumtyp.h \
 /opt/PV-7.0.0/prog/include/acqutyp.h \
 /opt/PV-7.0.0/prog/include/recotyp.h \
 /opt/PV-7.0.0/prog/include/Reco/RecoStageTyp.h \
 /opt/PV-7.0.0/prog/include/subjtyp.h \
 /opt/PV-7.0.0/prog/include/generated/ParxDefs.h \
 /opt/PV-7.0.0/prog/include/methodTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/publicPvmTypes.h \
 /opt/PV-7.0.0/prog/include/Parx/Dynenum.h \
 /opt/PV-7.0.0/prog/include/lib/PvSysinfoClient_state.h \
 /opt/PV-7.0.0/prog/include/adjManagerTypes.h \
 /opt/PV-7.0.0/prog/include/bruktyp.h \
 /opt/PV-7.0.0/prog/include/PvUtil/TimeDefs.h \
 /opt/PV-7.0.0/prog/include/adjManagerDefs.h \
 /opt/PV-7.0.0/prog/include/generated/DataPath.h \
 /opt/PV-7.0.0/prog/include/generated/KeyUids.h \
 /opt/PV-7.0.0/prog/include/PvCfg/ResultParsType.h \
 /opt/PV-7.0.0/prog/include/adjManagerDefs.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/geotyp.h \
 /opt/PV-7.0.0/prog/include/pvidl.h \
 /opt/PV-7.0.0/prog/include/generated/GeoObjIds.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/ReportTypes.h \
 /opt/PV-7.0.0/prog/include/generated/ParImport.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/wobbleTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/epiTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/SpiralTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/TrajectoryTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/internalPvmTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/SpiralTypesInternal.h \
 /opt/PV-7.0.0/prog/include/PVMTools.h \
 /opt/PV-7.0.0/prog/include/recotyp.h \
 /opt/PV-7.0.0/prog/include/PvUtilTools.h \
 /opt/PV-7.0.0/prog/include/PvUtil/PvExcept.hh \
 /opt/PV-7.0.0/prog/include/PvUtil/PvUtil.h \
 /opt/PV-7.0.0/prog/include/machine.h \
 /opt/PV-7.0.0/prog/include/Parx/RelationContext.hh \
 /opt/PV-7.0.0/prog/include/PvAcqTools.h \
 /opt/PV-7.0.0/prog/include/PvCfgTools.h \
 /opt/PV-7.0.0/prog/include/PvCfg/PvCfg.h \
 /opt/PV-7.0.0/prog/include/PvGeoTools.h \
 /opt/PV-7.0.0/prog/include/PvGeoClasses.hh \
 /opt/PV-7.0.0/prog/include/PvmTypes/geotyp.h \
 /opt/PV-7.0.0/prog/include/libCore/Math/Matrix3x3.hh \
 /opt/PV-7.0.0/prog/include/libCore/Containers/Exceptions/MathException.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/ExceptionBase.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/Core.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/internal.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/String.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/CoreString.h \
 /opt/PV-7.0.0/prog/include/libCore/Base/TimeStamped.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/TimeStamp.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/AutoPtr.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/Cleanup.hh \
 /opt/PV-7.0.0/prog/include/PvMrTools.h \
 /opt/PV-7.0.0/prog/include/PvPvmTools.h \
 /opt/PV-7.0.0/prog/include/adjManagerTypes.h \
 /opt/PV-7.0.0/prog/include/PvSeqTools.h \
 /opt/PV-7.0.0/prog/include/ovl_toolbox/RecoTools.h \
 /opt/PV-7.0.0/prog/include/ovl_toolbox/VisuTools.h \
 /opt/PV-7.0.0/prog/include/ovl_toolbox/Utils.h \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/parsTypes.h \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/relProtos.h \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/relProtos_p.h
BaseLevelRelations.o: \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/BaseLevelRelations.c \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/method.h \
 /opt/PV-7.0.0/prog/include/pvmachine.h \
 /opt/PV-7.0.0/prog/include/machine.h /opt/PV-7.0.0/prog/include/pvidl.h \
 /opt/PV-7.0.0/prog/include/generated/PvSystem.h \
 /opt/PV-7.0.0/prog/include/brukdef.h \
 /opt/PV-7.0.0/prog/include/bruktyp.h \
 /opt/PV-7.0.0/prog/include/Parx/Parx.h \
 /opt/PV-7.0.0/prog/include/Parx/publicTypes.h \
 /opt/PV-7.0.0/prog/include/PvUtil/TimeDefs.h \
 /opt/PV-7.0.0/prog/include/generated/ParxDefs.h \
 /opt/PV-7.0.0/prog/include/Parx/libParxRels.h \
 /opt/PV-7.0.0/prog/include/Parx/ovlinterface.h \
 /opt/PV-7.0.0/prog/include/Parx/pardef.h \
 /opt/PV-7.0.0/prog/include/generated/KeyUids.h \
 /opt/PV-7.0.0/prog/include/PvUtil/perfhash.h \
 /opt/PV-7.0.0/prog/include/ta_config.h \
 /opt/PV-7.0.0/prog/include/Parx/Dynenum.h \
 /opt/PV-7.0.0/prog/include/debugdef.h \
 /opt/PV-7.0.0/prog/include/lib/libPvPath.h \
 /opt/PV-7.0.0/prog/include/generated/DataPath.h \
 /opt/PV-7.0.0/prog/include/acqutyp.h \
 /opt/PV-7.0.0/prog/include/gradient_ramp_typ.h \
 /opt/PV-7.0.0/prog/include/acqumtyp.h \
 /opt/PV-7.0.0/prog/include/acqutyp.h \
 /opt/PV-7.0.0/prog/include/recotyp.h \
 /opt/PV-7.0.0/prog/include/Reco/RecoStageTyp.h \
 /opt/PV-7.0.0/prog/include/subjtyp.h \
 /opt/PV-7.0.0/prog/include/generated/ParxDefs.h \
 /opt/PV-7.0.0/prog/include/methodTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/publicPvmTypes.h \
 /opt/PV-7.0.0/prog/include/Parx/Dynenum.h \
 /opt/PV-7.0.0/prog/include/lib/PvSysinfoClient_state.h \
 /opt/PV-7.0.0/prog/include/adjManagerTypes.h \
 /opt/PV-7.0.0/prog/include/bruktyp.h \
 /opt/PV-7.0.0/prog/include/PvUtil/TimeDefs.h \
 /opt/PV-7.0.0/prog/include/adjManagerDefs.h \
 /opt/PV-7.0.0/prog/include/generated/DataPath.h \
 /opt/PV-7.0.0/prog/include/generated/KeyUids.h \
 /opt/PV-7.0.0/prog/include/PvCfg/ResultParsType.h \
 /opt/PV-7.0.0/prog/include/adjManagerDefs.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/geotyp.h \
 /opt/PV-7.0.0/prog/include/pvidl.h \
 /opt/PV-7.0.0/prog/include/generated/GeoObjIds.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/ReportTypes.h \
 /opt/PV-7.0.0/prog/include/generated/ParImport.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/wobbleTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/epiTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/SpiralTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/TrajectoryTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/internalPvmTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/SpiralTypesInternal.h \
 /opt/PV-7.0.0/prog/include/PVMTools.h \
 /opt/PV-7.0.0/prog/include/recotyp.h \
 /opt/PV-7.0.0/prog/include/PvUtilTools.h \
 /opt/PV-7.0.0/prog/include/PvUtil/PvExcept.hh \
 /opt/PV-7.0.0/prog/include/PvUtil/PvUtil.h \
 /opt/PV-7.0.0/prog/include/machine.h \
 /opt/PV-7.0.0/prog/include/Parx/RelationContext.hh \
 /opt/PV-7.0.0/prog/include/PvAcqTools.h \
 /opt/PV-7.0.0/prog/include/PvCfgTools.h \
 /opt/PV-7.0.0/prog/include/PvCfg/PvCfg.h \
 /opt/PV-7.0.0/prog/include/PvGeoTools.h \
 /opt/PV-7.0.0/prog/include/PvGeoClasses.hh \
 /opt/PV-7.0.0/prog/include/PvmTypes/geotyp.h \
 /opt/PV-7.0.0/prog/include/libCore/Math/Matrix3x3.hh \
 /opt/PV-7.0.0/prog/include/libCore/Containers/Exceptions/MathException.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/ExceptionBase.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/Core.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/internal.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/String.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/CoreString.h \
 /opt/PV-7.0.0/prog/include/libCore/Base/TimeStamped.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/TimeStamp.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/AutoPtr.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/Cleanup.hh \
 /opt/PV-7.0.0/prog/include/PvMrTools.h \
 /opt/PV-7.0.0/prog/include/PvPvmTools.h \
 /opt/PV-7.0.0/prog/include/adjManagerTypes.h \
 /opt/PV-7.0.0/prog/include/PvSeqTools.h \
 /opt/PV-7.0.0/prog/include/ovl_toolbox/RecoTools.h \
 /opt/PV-7.0.0/prog/include/ovl_toolbox/VisuTools.h \
 /opt/PV-7.0.0/prog/include/ovl_toolbox/Utils.h \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/parsTypes.h \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/relProtos.h \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/relProtos_p.h
RecoRelations.o: \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/RecoRelations.c \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/method.h \
 /opt/PV-7.0.0/prog/include/pvmachine.h \
 /opt/PV-7.0.0/prog/include/machine.h /opt/PV-7.0.0/prog/include/pvidl.h \
 /opt/PV-7.0.0/prog/include/generated/PvSystem.h \
 /opt/PV-7.0.0/prog/include/brukdef.h \
 /opt/PV-7.0.0/prog/include/bruktyp.h \
 /opt/PV-7.0.0/prog/include/Parx/Parx.h \
 /opt/PV-7.0.0/prog/include/Parx/publicTypes.h \
 /opt/PV-7.0.0/prog/include/PvUtil/TimeDefs.h \
 /opt/PV-7.0.0/prog/include/generated/ParxDefs.h \
 /opt/PV-7.0.0/prog/include/Parx/libParxRels.h \
 /opt/PV-7.0.0/prog/include/Parx/ovlinterface.h \
 /opt/PV-7.0.0/prog/include/Parx/pardef.h \
 /opt/PV-7.0.0/prog/include/generated/KeyUids.h \
 /opt/PV-7.0.0/prog/include/PvUtil/perfhash.h \
 /opt/PV-7.0.0/prog/include/ta_config.h \
 /opt/PV-7.0.0/prog/include/Parx/Dynenum.h \
 /opt/PV-7.0.0/prog/include/debugdef.h \
 /opt/PV-7.0.0/prog/include/lib/libPvPath.h \
 /opt/PV-7.0.0/prog/include/generated/DataPath.h \
 /opt/PV-7.0.0/prog/include/acqutyp.h \
 /opt/PV-7.0.0/prog/include/gradient_ramp_typ.h \
 /opt/PV-7.0.0/prog/include/acqumtyp.h \
 /opt/PV-7.0.0/prog/include/acqutyp.h \
 /opt/PV-7.0.0/prog/include/recotyp.h \
 /opt/PV-7.0.0/prog/include/Reco/RecoStageTyp.h \
 /opt/PV-7.0.0/prog/include/subjtyp.h \
 /opt/PV-7.0.0/prog/include/generated/ParxDefs.h \
 /opt/PV-7.0.0/prog/include/methodTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/publicPvmTypes.h \
 /opt/PV-7.0.0/prog/include/Parx/Dynenum.h \
 /opt/PV-7.0.0/prog/include/lib/PvSysinfoClient_state.h \
 /opt/PV-7.0.0/prog/include/adjManagerTypes.h \
 /opt/PV-7.0.0/prog/include/bruktyp.h \
 /opt/PV-7.0.0/prog/include/PvUtil/TimeDefs.h \
 /opt/PV-7.0.0/prog/include/adjManagerDefs.h \
 /opt/PV-7.0.0/prog/include/generated/DataPath.h \
 /opt/PV-7.0.0/prog/include/generated/KeyUids.h \
 /opt/PV-7.0.0/prog/include/PvCfg/ResultParsType.h \
 /opt/PV-7.0.0/prog/include/adjManagerDefs.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/geotyp.h \
 /opt/PV-7.0.0/prog/include/pvidl.h \
 /opt/PV-7.0.0/prog/include/generated/GeoObjIds.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/ReportTypes.h \
 /opt/PV-7.0.0/prog/include/generated/ParImport.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/wobbleTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/epiTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/SpiralTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/TrajectoryTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/internalPvmTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/SpiralTypesInternal.h \
 /opt/PV-7.0.0/prog/include/PVMTools.h \
 /opt/PV-7.0.0/prog/include/recotyp.h \
 /opt/PV-7.0.0/prog/include/PvUtilTools.h \
 /opt/PV-7.0.0/prog/include/PvUtil/PvExcept.hh \
 /opt/PV-7.0.0/prog/include/PvUtil/PvUtil.h \
 /opt/PV-7.0.0/prog/include/machine.h \
 /opt/PV-7.0.0/prog/include/Parx/RelationContext.hh \
 /opt/PV-7.0.0/prog/include/PvAcqTools.h \
 /opt/PV-7.0.0/prog/include/PvCfgTools.h \
 /opt/PV-7.0.0/prog/include/PvCfg/PvCfg.h \
 /opt/PV-7.0.0/prog/include/PvGeoTools.h \
 /opt/PV-7.0.0/prog/include/PvGeoClasses.hh \
 /opt/PV-7.0.0/prog/include/PvmTypes/geotyp.h \
 /opt/PV-7.0.0/prog/include/libCore/Math/Matrix3x3.hh \
 /opt/PV-7.0.0/prog/include/libCore/Containers/Exceptions/MathException.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/ExceptionBase.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/Core.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/internal.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/String.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/CoreString.h \
 /opt/PV-7.0.0/prog/include/libCore/Base/TimeStamped.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/TimeStamp.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/AutoPtr.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/Cleanup.hh \
 /opt/PV-7.0.0/prog/include/PvMrTools.h \
 /opt/PV-7.0.0/prog/include/PvPvmTools.h \
 /opt/PV-7.0.0/prog/include/adjManagerTypes.h \
 /opt/PV-7.0.0/prog/include/PvSeqTools.h \
 /opt/PV-7.0.0/prog/include/ovl_toolbox/RecoTools.h \
 /opt/PV-7.0.0/prog/include/ovl_toolbox/VisuTools.h \
 /opt/PV-7.0.0/prog/include/ovl_toolbox/Utils.h \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/parsTypes.h \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/relProtos.h \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/relProtos_p.h
backbone.o: \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/backbone.c \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/method.h \
 /opt/PV-7.0.0/prog/include/pvmachine.h \
 /opt/PV-7.0.0/prog/include/machine.h /opt/PV-7.0.0/prog/include/pvidl.h \
 /opt/PV-7.0.0/prog/include/generated/PvSystem.h \
 /opt/PV-7.0.0/prog/include/brukdef.h \
 /opt/PV-7.0.0/prog/include/bruktyp.h \
 /opt/PV-7.0.0/prog/include/Parx/Parx.h \
 /opt/PV-7.0.0/prog/include/Parx/publicTypes.h \
 /opt/PV-7.0.0/prog/include/PvUtil/TimeDefs.h \
 /opt/PV-7.0.0/prog/include/generated/ParxDefs.h \
 /opt/PV-7.0.0/prog/include/Parx/libParxRels.h \
 /opt/PV-7.0.0/prog/include/Parx/ovlinterface.h \
 /opt/PV-7.0.0/prog/include/Parx/pardef.h \
 /opt/PV-7.0.0/prog/include/generated/KeyUids.h \
 /opt/PV-7.0.0/prog/include/PvUtil/perfhash.h \
 /opt/PV-7.0.0/prog/include/ta_config.h \
 /opt/PV-7.0.0/prog/include/Parx/Dynenum.h \
 /opt/PV-7.0.0/prog/include/debugdef.h \
 /opt/PV-7.0.0/prog/include/lib/libPvPath.h \
 /opt/PV-7.0.0/prog/include/generated/DataPath.h \
 /opt/PV-7.0.0/prog/include/acqutyp.h \
 /opt/PV-7.0.0/prog/include/gradient_ramp_typ.h \
 /opt/PV-7.0.0/prog/include/acqumtyp.h \
 /opt/PV-7.0.0/prog/include/acqutyp.h \
 /opt/PV-7.0.0/prog/include/recotyp.h \
 /opt/PV-7.0.0/prog/include/Reco/RecoStageTyp.h \
 /opt/PV-7.0.0/prog/include/subjtyp.h \
 /opt/PV-7.0.0/prog/include/generated/ParxDefs.h \
 /opt/PV-7.0.0/prog/include/methodTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/publicPvmTypes.h \
 /opt/PV-7.0.0/prog/include/Parx/Dynenum.h \
 /opt/PV-7.0.0/prog/include/lib/PvSysinfoClient_state.h \
 /opt/PV-7.0.0/prog/include/adjManagerTypes.h \
 /opt/PV-7.0.0/prog/include/bruktyp.h \
 /opt/PV-7.0.0/prog/include/PvUtil/TimeDefs.h \
 /opt/PV-7.0.0/prog/include/adjManagerDefs.h \
 /opt/PV-7.0.0/prog/include/generated/DataPath.h \
 /opt/PV-7.0.0/prog/include/generated/KeyUids.h \
 /opt/PV-7.0.0/prog/include/PvCfg/ResultParsType.h \
 /opt/PV-7.0.0/prog/include/adjManagerDefs.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/geotyp.h \
 /opt/PV-7.0.0/prog/include/pvidl.h \
 /opt/PV-7.0.0/prog/include/generated/GeoObjIds.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/ReportTypes.h \
 /opt/PV-7.0.0/prog/include/generated/ParImport.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/wobbleTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/epiTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/SpiralTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/TrajectoryTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/internalPvmTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/SpiralTypesInternal.h \
 /opt/PV-7.0.0/prog/include/PVMTools.h \
 /opt/PV-7.0.0/prog/include/recotyp.h \
 /opt/PV-7.0.0/prog/include/PvUtilTools.h \
 /opt/PV-7.0.0/prog/include/PvUtil/PvExcept.hh \
 /opt/PV-7.0.0/prog/include/PvUtil/PvUtil.h \
 /opt/PV-7.0.0/prog/include/machine.h \
 /opt/PV-7.0.0/prog/include/Parx/RelationContext.hh \
 /opt/PV-7.0.0/prog/include/PvAcqTools.h \
 /opt/PV-7.0.0/prog/include/PvCfgTools.h \
 /opt/PV-7.0.0/prog/include/PvCfg/PvCfg.h \
 /opt/PV-7.0.0/prog/include/PvGeoTools.h \
 /opt/PV-7.0.0/prog/include/PvGeoClasses.hh \
 /opt/PV-7.0.0/prog/include/PvmTypes/geotyp.h \
 /opt/PV-7.0.0/prog/include/libCore/Math/Matrix3x3.hh \
 /opt/PV-7.0.0/prog/include/libCore/Containers/Exceptions/MathException.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/ExceptionBase.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/Core.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/internal.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/String.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/CoreString.h \
 /opt/PV-7.0.0/prog/include/libCore/Base/TimeStamped.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/TimeStamp.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/AutoPtr.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/Cleanup.hh \
 /opt/PV-7.0.0/prog/include/PvMrTools.h \
 /opt/PV-7.0.0/prog/include/PvPvmTools.h \
 /opt/PV-7.0.0/prog/include/adjManagerTypes.h \
 /opt/PV-7.0.0/prog/include/PvSeqTools.h \
 /opt/PV-7.0.0/prog/include/ovl_toolbox/RecoTools.h \
 /opt/PV-7.0.0/prog/include/ovl_toolbox/VisuTools.h \
 /opt/PV-7.0.0/prog/include/ovl_toolbox/Utils.h \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/parsTypes.h \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/relProtos.h \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/relProtos_p.h
deriveVisu.o: \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/deriveVisu.c \
 /opt/PV-7.0.0/prog/include/machine.h \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/method.h \
 /opt/PV-7.0.0/prog/include/pvmachine.h \
 /opt/PV-7.0.0/prog/include/machine.h /opt/PV-7.0.0/prog/include/pvidl.h \
 /opt/PV-7.0.0/prog/include/generated/PvSystem.h \
 /opt/PV-7.0.0/prog/include/brukdef.h \
 /opt/PV-7.0.0/prog/include/bruktyp.h \
 /opt/PV-7.0.0/prog/include/Parx/Parx.h \
 /opt/PV-7.0.0/prog/include/Parx/publicTypes.h \
 /opt/PV-7.0.0/prog/include/PvUtil/TimeDefs.h \
 /opt/PV-7.0.0/prog/include/generated/ParxDefs.h \
 /opt/PV-7.0.0/prog/include/Parx/libParxRels.h \
 /opt/PV-7.0.0/prog/include/Parx/ovlinterface.h \
 /opt/PV-7.0.0/prog/include/Parx/pardef.h \
 /opt/PV-7.0.0/prog/include/generated/KeyUids.h \
 /opt/PV-7.0.0/prog/include/PvUtil/perfhash.h \
 /opt/PV-7.0.0/prog/include/ta_config.h \
 /opt/PV-7.0.0/prog/include/Parx/Dynenum.h \
 /opt/PV-7.0.0/prog/include/debugdef.h \
 /opt/PV-7.0.0/prog/include/lib/libPvPath.h \
 /opt/PV-7.0.0/prog/include/generated/DataPath.h \
 /opt/PV-7.0.0/prog/include/acqutyp.h \
 /opt/PV-7.0.0/prog/include/gradient_ramp_typ.h \
 /opt/PV-7.0.0/prog/include/acqumtyp.h \
 /opt/PV-7.0.0/prog/include/acqutyp.h \
 /opt/PV-7.0.0/prog/include/recotyp.h \
 /opt/PV-7.0.0/prog/include/Reco/RecoStageTyp.h \
 /opt/PV-7.0.0/prog/include/subjtyp.h \
 /opt/PV-7.0.0/prog/include/generated/ParxDefs.h \
 /opt/PV-7.0.0/prog/include/methodTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/publicPvmTypes.h \
 /opt/PV-7.0.0/prog/include/Parx/Dynenum.h \
 /opt/PV-7.0.0/prog/include/lib/PvSysinfoClient_state.h \
 /opt/PV-7.0.0/prog/include/adjManagerTypes.h \
 /opt/PV-7.0.0/prog/include/bruktyp.h \
 /opt/PV-7.0.0/prog/include/PvUtil/TimeDefs.h \
 /opt/PV-7.0.0/prog/include/adjManagerDefs.h \
 /opt/PV-7.0.0/prog/include/generated/DataPath.h \
 /opt/PV-7.0.0/prog/include/generated/KeyUids.h \
 /opt/PV-7.0.0/prog/include/PvCfg/ResultParsType.h \
 /opt/PV-7.0.0/prog/include/adjManagerDefs.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/geotyp.h \
 /opt/PV-7.0.0/prog/include/pvidl.h \
 /opt/PV-7.0.0/prog/include/generated/GeoObjIds.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/ReportTypes.h \
 /opt/PV-7.0.0/prog/include/generated/ParImport.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/wobbleTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/epiTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/SpiralTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/TrajectoryTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/internalPvmTypes.h \
 /opt/PV-7.0.0/prog/include/PvmTypes/SpiralTypesInternal.h \
 /opt/PV-7.0.0/prog/include/PVMTools.h \
 /opt/PV-7.0.0/prog/include/recotyp.h \
 /opt/PV-7.0.0/prog/include/PvUtilTools.h \
 /opt/PV-7.0.0/prog/include/PvUtil/PvExcept.hh \
 /opt/PV-7.0.0/prog/include/PvUtil/PvUtil.h \
 /opt/PV-7.0.0/prog/include/Parx/RelationContext.hh \
 /opt/PV-7.0.0/prog/include/PvAcqTools.h \
 /opt/PV-7.0.0/prog/include/PvCfgTools.h \
 /opt/PV-7.0.0/prog/include/PvCfg/PvCfg.h \
 /opt/PV-7.0.0/prog/include/PvGeoTools.h \
 /opt/PV-7.0.0/prog/include/PvGeoClasses.hh \
 /opt/PV-7.0.0/prog/include/PvmTypes/geotyp.h \
 /opt/PV-7.0.0/prog/include/libCore/Math/Matrix3x3.hh \
 /opt/PV-7.0.0/prog/include/libCore/Containers/Exceptions/MathException.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/ExceptionBase.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/Core.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/internal.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/String.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/CoreString.h \
 /opt/PV-7.0.0/prog/include/libCore/Base/TimeStamped.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/TimeStamp.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/AutoPtr.hh \
 /opt/PV-7.0.0/prog/include/libCore/Base/Cleanup.hh \
 /opt/PV-7.0.0/prog/include/PvMrTools.h \
 /opt/PV-7.0.0/prog/include/PvPvmTools.h \
 /opt/PV-7.0.0/prog/include/adjManagerTypes.h \
 /opt/PV-7.0.0/prog/include/PvSeqTools.h \
 /opt/PV-7.0.0/prog/include/ovl_toolbox/RecoTools.h \
 /opt/PV-7.0.0/prog/include/ovl_toolbox/VisuTools.h \
 /opt/PV-7.0.0/prog/include/ovl_toolbox/Utils.h \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/parsTypes.h \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/relProtos.h \
 /opt/PV-7.0.0/prog/curdir/nmr/ParaVision/methods/src/dianaFLASH/relProtos_p.h \
 /opt/PV-7.0.0/prog/include/Visu/VisuTypes.h \
 /opt/PV-7.0.0/prog/include/Visu/VisuDefines.h \
 /opt/PV-7.0.0/prog/include/generated/VisuIds.h

