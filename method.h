/****************************************************************
 *
 * $Source$
 *
 * Copyright (c) 2018
 * Bruker BioSpin MRI GmbH
 * D-76275 Ettlingen, Germany
 *
 * All Rights Reserved
 *
 * $Id$
 *
 ****************************************************************/
#ifndef METHOD_H
#define METHOD_H

/*--------------------------------------------------------------*
 * Prototypes of relations functions...
 *--------------------------------------------------------------*/


/* --------------------------------------------------------------
 *   Include files...
 *--------------------------------------------------------------*/

#include "pvmachine.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>

/*--------------------------------------------------------------*
 * include files required at the start by Parx...
 *--------------------------------------------------------------*/

#include "brukdef.h"
#include "bruktyp.h"
#include "Parx/Parx.h"

/*--------------------------------------------------------------*
 * Uxnmr include files...
 *--------------------------------------------------------------*/

#include "ta_config.h"
#include "debugdef.h"
#include "lib/libPvPath.h"


#include "acqutyp.h"
#include "acqumtyp.h"
#include "recotyp.h"
#include "subjtyp.h"
#include "methodTypes.h"


/*--------------------------------------------------------------*
 * Global include files...
 *--------------------------------------------------------------*/

#include "PVMTools.h"

/*--------------------------------------------------------------*
 * Method include files...
 *--------------------------------------------------------------*/

#include "parsTypes.h"
#include "relProtos.h"



#endif

/****************************************************************/
/*	E N D   O F   F I L E					*/
/****************************************************************/

